import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Dimensions } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import SchedulerForm from './components/SchedulerForm';
import Menu from './components/Menu';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';



const Stack = createStackNavigator();


export default class App extends Component {

  constructor() {
    super();
    const { width } = Dimensions.get('window');
    global.windowWidth = width - 100;
  }

  state = {
    date: new Date(1598051730000),
    mode: 'date',
    show: false
  }

  onChange = (event, selectedDate) => {
    let currentDate = selectedDate || this.state.date;
    this.setState({ show: true });
    this.setState({ date: currentDate });
  };

  showMode = (currentMode) => {
    this.setState({ show: true });
    this.setState({ mode: currentMode });
  };

  showDatepicker = () => {
    this.showMode('date');
  };

  showTimepicker = () => {
    this.showMode('time');
  };

  render = () => {
    return <NavigationContainer>
      <View style={styles.container}>
        <Stack.Navigator>
          <Stack.Screen name="Menu" component={Menu} />
          <Stack.Screen
            name="Scheduler"
            component={SchedulerForm}
          />
        </Stack.Navigator>
      </View>
    </NavigationContainer>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});
