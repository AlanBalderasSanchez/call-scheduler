
import React, { Component } from 'react';
import { StyleSheet, View, TextInput, Button, Text, CheckBox, Keyboard } from 'react-native';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import { TouchableOpacity } from 'react-native-gesture-handler';


//TODO: Solve the issue when the timer gets closed it pops up again
export default class TimePicker extends Component {

    state = {
        showTimePicker: false
    };


    showTimePicker = () => {
        console.log("Show time picker!");
        this.setState({ showTimePicker: true })
    }

    onTimeChangeHandler = (event, value) => {
        this.props.onTimeChangeHandler(event, value);
        this.setState({ showTimePicker: false });
    }

    render() {

        const timeFormat = new Date(this.props.time);

        return <View>
            <TouchableOpacity onPress={this.showTimePicker}>
                <Text>{timeFormat.getHours() + ":" + timeFormat.getMinutes()}</Text>
            </TouchableOpacity>
            {
                this.state.showTimePicker && (
                    <RNDateTimePicker
                        value={timeFormat}
                        mode={'time'}
                        is24Hour={true}
                        display="default"
                        onChange={(event, value) => { this.onTimeChangeHandler(event, value); }}
                    />
                )
            }
        </View >
    }
}