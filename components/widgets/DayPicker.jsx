import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const DayPicker = (props) => {

    return <View style={styles.daysContainer}>
        {
            props.days.map((day) => {

                const dayStyles = [styles.day];

                if (day.checked) {
                    dayStyles.push(styles.selectedDay);
                }

                return <TouchableOpacity key={day.dayName} onPress={() => props.onDayPressHandler(day.dayName)}>
                    <View style={[dayStyles]}>
                        <Text style={styles.dayFont}>{day.dayName}</Text>
                    </View>
                </TouchableOpacity >
            })
        }
    </View>;



}

export default DayPicker;

const styles = StyleSheet.create({
    daysContainer: {
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "space-between",
        height: 50,
        width: global.windowWidth
    },
    day: {
        marginBottom: 10
    },
    dayFont: {
        fontSize: 20
    },
    selectedDay: {
        backgroundColor: "#717bbf"
    }
})