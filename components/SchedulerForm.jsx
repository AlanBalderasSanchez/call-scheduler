
import React, { Component } from 'react';
import { StyleSheet, View, TextInput, Button, Text, CheckBox, Keyboard } from 'react-native';
import DayPicker from './widgets/DayPicker';
import TimePicker from './widgets/TimePicker';
import Storing from '../util/Storing';

export default class SchedulerForm extends Component {

    constructor() {
        super();
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }
    componentWillUnmount() {
        this.keyboardDidHideListener.remove();
        this._unsubscribe();

    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
           


        });

        try {
            const scheduleId = this.props.route.param.scheduleId;
            console.log("schedule id:", scheduleId);
            if (typeof scheduleId != "undefined" && scheduleId != null) {
                Storing.getSchedule(scheduleId)
                    .then((obj) => {
                        if (obj != null)
                            this.setState({ ...obj });
                    })
                    .catch((error) => {
                    });
            }

        } catch (error) {
        }


    }

    state = {
        reminderLabel: "",
        contactName: "",
        repeatEveryDay: false,
        days: [
            { dayName: "Monday", checked: false },
            { dayName: "Tuesday", checked: false },
            { dayName: "Wednesday", checked: false },
            { dayName: "Thursday", checked: false },
            { dayName: "Friday", checked: false },
            { dayName: "Saturday", checked: false },
            { dayName: "Sunday", checked: false }],
        repeatTime: Date.now(),
        phone: ""
    };

    onDayPressHandler = (id) => {
        const dayIndex = this.state.days.findIndex(day => day.dayName == id);
        const day = { ...this.state.days[dayIndex] };
        day.checked = !day.checked;
        const days = [...this.state.days];
        days[dayIndex] = day;
        this.setState({ days: days });
    };

    onTimeChangeHandler = (event, newTime) => {
        this.setState({ repeatTime: newTime });
    };

    changeDayValue = (value, id) => {
        const dayIndex = this.state.days.findIndex(day => day.dayName == id);
        const day = { ...this.state.days[dayIndex] };
        day.checked = value;
        const days = [...this.state.days];
        days[dayIndex] = day;
        this.setState({ days: days });
    };

    onRepeatEveryDayChange = value => {
        this.setState({ repeatEveryDay: value });
    }

    onReminderLabelChange = value => {
        this.setState({ reminderLabel: value });
    };

    _keyboardDidHide() {
        console.log('Keyboard Hidden');
        Keyboard.dismiss;
    }

    onSaveButtomPressed = () => {
        const newSchedule = { ...this.state };
        Storing.saveSchedule(newSchedule);
        this.props.navigation.navigate('Menu', {});
    }

    onContactNameChange = (contactName) => {
        this.setState({ contactName: contactName });
    }

    render() {
        if (typeof this.state.repeatTime == "undefined")
            this.setState({ repeatTime: new Date() });

        return <View style={styles.container}>

            <Text style={styles.spacer}>Reminder Label</Text>
            <TextInput placeholder="Alias for this reminder" style={styles.textInputStyle} value={this.state.reminderLabel} onChangeText={this.onReminderLabelChange}></TextInput>

            <Text style={styles.spacer}>Contact Name</Text>
            <TextInput placeholder="Contact name" style={styles.textInputStyle} value={this.state.contactName} onChangeText={this.onContactNameChange}></TextInput>

            <Text>Phone Number</Text>
            <TextInput
                style={styles.textInputStyle}
                placeholder="Phone to dial"
                underlineColorAndroid='transparent'
                keyboardType='phone-pad'
                value={this.state.phone}
                onChangeText={(value) => { this.setState({ phone: value }) }}
            />

            <Text style={styles.spacer}>Time</Text>
            <TimePicker onTimeChangeHandler={this.onTimeChangeHandler} time={this.state.repeatTime} />

            <View style={[styles.formElement, styles.checkboxContainer]}>
                <CheckBox style={styles.checkbox} value={this.state.repeatEveryDay} onValueChange={this.onRepeatEveryDayChange} />
                <Text style={styles.label}>Repeat every day</Text>
            </View>

            {(!this.state.repeatEveryDay &&
                <DayPicker days={this.state.days} onDayPressHandler={this.onDayPressHandler} />)}

            <View style={styles.saveButton}>
                <Button title="Save button" onPress={this.onSaveButtomPressed}>Save</Button>
            </View>
        </View>
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        flexWrap: "wrap",
        backgroundColor: '#fff',
        padding: 20,
    },
    formElement: {
        marginTop: 50,
        height: 50,
        width: global.windowWidth,
        flexDirection: "row",
        flexWrap: "wrap"
    },
    textInputStyle: {
        borderBottomWidth: 1,
        width: 300,
    },
    checkboxContainer: {
        flexDirection: "row",
        marginBottom: 20
    },
    label: {
        margin: 8
    },
    checkbox: {
        alignSelf: "center",
    },
    saveButton: {
        alignSelf: "center",
        marginTop: 200
    }
})