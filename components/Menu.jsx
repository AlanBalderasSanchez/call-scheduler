import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import Storing from '../util/Storing';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { FlatList } from 'react-native-gesture-handler';

export default class Menu extends Component {

    state = {
        schedules: []
    };

    componentDidMount = () => {
        console.log("Component Menu did mount");

        try {
            Storing.getSchedules()
                .then((schedules) => {
                    this.setState({ schedules: schedules });
                })
        }
        catch (error) {
            console.log("The error:", error);
        }
        //const schedules = Storing.getSchedules();
        //this.setState({ schedules: schedules });
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            Storing.getSchedules()
                .then((schedules) => {
                    console.log("Schedules:", schedules);
                    if (schedules != null)
                        this.setState({ schedules: [...schedules] });

                    else this.setState({ schedules: [] });
                })
                .catch((error) => {

                });
        });
    };

    onCreateButtonPress = () => {
        this.props.navigation.navigate('Scheduler', {
        });

        this._unsubscribe();
    }

    renderSchedule = (schedule) => {
        return <Text>
            {schedule.reminderLabel}
        </Text>;
    }

    onScheduleItemPress = (scheduleId) => {
        this.props.navigation.navigate('Scheduler', {
            schedulerId: scheduleId
        });
    };

    render() {

        return <View style={styles.container}>
            <Text style={styles.title}> Schedule your calls! </Text>

            <Button style={styles.button}
                onPress={this.onCreateButtonPress} title="Create New">Create New</Button>

            <View>
                {
                    this.state.schedules.map((schedule) => {
                        return <TouchableOpacity key={schedule.id} onPress={() => this.onScheduleItemPress(schedule.id)}>
                            <Text>{schedule.reminderLabel}</Text>
                        </TouchableOpacity>
                    })
                }
            </View>
        </View>
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#ffffff",
        flexDirection: "column"
    },
    title: {
        alignSelf: "center",
        margin: 10,
        fontSize: 30
    },
    button: {
        width: 20,
        marginTop: 100,
        alignSelf: "center",
        backgroundColor: "#2f5069"
    }
});