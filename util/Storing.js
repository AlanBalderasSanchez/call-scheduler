import * as SQLite from 'expo-sqlite';

export default class Storing {

    instance;
    db;

    db_data = {
        name: "phone_schedule.db",
        version: 1.0,
        description: "Database to store scheduled calls",
        size: 100
    }

    constructor() {
        this.db = SQLite.openDatabase(this.db_data.name, this.db_data.version, this.db_data.description, this.db_data.size);
        console.log("Database:", this.db);
        this.db.transaction((tx) => {
            tx.executeSql(`CREATE TABLE IF NOT EXISTS schedules(
                id integer primary key,
                schedule_label text not null,
                phone text not null,
                contact_name text,
                monday boolean not null,
                tuesday boolean not null,
                wednesday boolean not null,
                thursday boolean not null,
                friday boolean not null,
                saturday boolean not null,
                sunday boolean not null,
                repeat_every_day boolean not null,
                repeat_time integer not null
            );`)
        });


    }

    static getInstance = () => {

        if (this.instance === undefined || this.instance == null) {
            this.instance = new Storing();
        }

        return this.instance;
    }

    //Implement: 
    // getSchedules
    // getSchedule
    // saveSchedule

    static getDaysQuery = days => {
        return days.reduce((query, day) => {
            console.log(query);
            return (query).concat(`${(day.checked) ? 1 : 0},`);
        }, "");
    }



    static saveSchedule = (schedule) => {
        const promise = new Promise((resolve, reject) => {
            Storing.getInstance().db.transaction((tx) => {
                const query = `INSERT INTO schedules (schedule_label, phone, contact_name,monday,tuesday,wednesday,thursday,friday,saturday,sunday,repeat_every_day,repeat_time)
                values (
                    "${schedule.reminderLabel}" , 
                    "${schedule.phone}",
                     "${schedule.contactName}",
                    ${this.getDaysQuery(schedule.days)}
                    ${(schedule.repeatEveryDay) ? 1 : 0}, 
                    ${schedule.repeatTime})`;

                tx.executeSql(
                    query
                    , []
                    , (tx, results) => {
                        console.log("Saved successfully:", results);
                        resolve(results);
                    },
                    (tx, error) => {
                        console.log("An error:", error);
                        throw new Error(error);
                    });

            });
        });
    }

    static getSchedules = () => {
        const promise = new Promise((resolve, reject) => {
            Storing.getInstance().db.transaction((tx) => {
                tx.executeSql('SELECT * FROM schedules', [], (tx, results) => {
                    const result = [];

                    for (var i = 0; i < results.rows.length; i++) {
                        result.push(this.mapRowToSchedule(results.rows.item(i)));
                    }

                    resolve(result);
                },
                    (tx, error) => {
                        reject(error);
                    });
            });
        });

        return promise;
    }


    static getSchedule = (scheduleId) => {
        const promise = new Promise((resolve, reject) => {
            Storing.getInstance().db.transaction((tx) => {
                tx.executeSql('SELECT * FROM schedules WHERE schedules.id =' + scheduleId, [], (tx, results) => {
                    if (results.length > 0)
                        resolve(this.mapRowToSchedule(tx.results.rows.item(0)));
                    else reject(null);
                });
            });
        });

        return promise;
    }

    static mapRowToSchedule = row => {
        return {
            id: row["id"],
            reminderLabel: row["schedule_label"],
            contactName: row["phone"],
            repeatEveryDay: (row["repeat_every_day"] == "1") ? true : false,
            days: [
                { dayName: "Monday", checked: ((row["monday"] == "1") ? true : false) },
                { dayName: "Tuesday", checked: ((row["tuesday"] == "1") ? true : false) },
                { dayName: "Wednesday", checked: ((row["wednesday"] == "1") ? true : false) },
                { dayName: "Thursday", checked: ((row["thursday"] == "1") ? true : false) },
                { dayName: "Friday", checked: ((row["friday"] == "1") ? true : false) },
                { dayName: "Saturday", checked: ((row["saturday"] == "1") ? true : false) },
                { dayName: "Sunday", checked: ((row["sunday"] == "1") ? true : false) }],
            repeatTime: (row["repeat_time"] == "1") ? true : false,
            phone: row["phone"]
        }
    }
}

